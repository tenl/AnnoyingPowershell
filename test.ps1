$PCLIST = Get-Content 'C:\TEST\PCLIST.TXT'

ForEach ($computer in $PCLIST) {

    Invoke-Command -ComputerName $computer -Scriptblock {
        $GetUserName = [Environment]::UserName
        $CmdMessage = {C:\windows\system32\msg.exe * 'Hello' $GetUserName 'This is a test!'}

        $CmdMessage | Invoke-Expression
    }

}
